#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <unordered_map>
#include <ctime> 
#include <pthread.h> 

using namespace std;

#define K 75
#define SIZE 500
#define MAX_PERCENT 35
#define MAX_ITERATIONS 250
#define MAX_THREADS 10

//Thread Variables
vector<pthread_t> threads;
struct ThreadArgs {
    int row;
    long id;
};

struct DataPoint {
    int x;
    int y;
    string cls;
};

struct CentreDataPoint {
    DataPoint point;
    int position;
};

//NOTE: ARRAY[ROW][COLUMN]
DataPoint MATRIX[SIZE][SIZE];
vector<DataPoint> Points;
DataPoint KPoints[K];
unordered_map<int, DataPoint[2]> MinMax;
volatile int CURRENT_POINT = 0;

void print_timediff(const char* prefix, const struct timespec& start, const struct timespec& end) {
    double milliseconds = end.tv_nsec >= start.tv_nsec
                        ? (end.tv_nsec - start.tv_nsec) / 1e6 + (end.tv_sec - start.tv_sec) * 1e3
                        : (start.tv_nsec - end.tv_nsec) / 1e6 + (end.tv_sec - start.tv_sec - 1) * 1e3;
    printf("%s: %lf seconds\n", prefix, (milliseconds / 1000));
}

void PopulateMatrix() {
    //Populate array randomly
    for (long i = 0; i < (SIZE * SIZE) * ((float) MAX_PERCENT) / 100; i++){
        DataPoint dp;
        dp.x = rand() % SIZE;
        dp.y = rand() % SIZE;
        dp.cls = " ";

        //Validation check to not add duplicated points
        if (MATRIX[dp.y][dp.x].cls != " ") {
            MATRIX[dp.y][dp.x] = dp;
            Points.push_back(dp);
        }
        
    }
}

void PrintMatrix() {
    //Open File to dump results
    ofstream myfile;
    myfile.open ("task 2 results.txt");

    for (int i = 0; i < SIZE; i++) {
        for (int z = 0; z < SIZE; z++) {

            //Write current value at MATRIX[i][z] location
            if (MATRIX[i][z].cls == "") {
                myfile << " , ";
            }
            else {
                myfile << MATRIX[i][z].cls;
                myfile << ", ";
            }
            
        }

        myfile << endl;
    }

    myfile.close();
}

int PointDistance(DataPoint &from, DataPoint &to) {
    int xDifference = to.x - from.x;
    int yDifference = to.y - from.y;
    return sqrt(pow(xDifference, 2) + pow(yDifference, 2));
}

CentreDataPoint SelectCentrePoint(int k, DataPoint &from, DataPoint &to) {
    DataPoint result;
    result.x = (from.x + to.x) / 2;
    result.y = (from.y + to.y) / 2;

    int minDistance = SIZE, minPosition = 0;

    for (int i = 0; i < Points.size(); i++) {
        int distance = PointDistance(result, Points[i]);

        if (distance < minDistance) {
            minDistance = distance;
            minPosition = i;
        }
    }

    CentreDataPoint centreResult;
    centreResult.point = Points[minPosition];
    centreResult.position = minPosition;

    return centreResult;
}

void SelectK() {
    if (MinMax.empty()) {
        //Selecting Inital K Points randomly
        for (int i = 0; i < K; i++) {
            //Set the cluster group in the points array
            int position = rand() % Points.size();
            Points[position].cls = to_string(i);

            //Update kpoints array with the selected point
            KPoints[i] = Points[position];

            //Update matrix to indicate the selected point as reference point
            MATRIX[KPoints[i].y][KPoints[i].x].cls = to_string(i);
            
            //Delete selected k point from original points array
            Points.erase(Points.begin() + position);
        }

        return;
    }

    for (int i = 0; i < K; i++) { 
        //Update matrix to indicate the selected point as reference point
        MATRIX[KPoints[i].y][KPoints[i].x].cls = to_string(i);
        
        //Add original k point back into points array
        Points.push_back(KPoints[i]);

        //Reset current k point with center point of its cluster
        CentreDataPoint centrePoint = SelectCentrePoint(i, MinMax[i][0], MinMax[i][1]);
        KPoints[i] = centrePoint.point;

        //Update matrix to indicate the selected point as reference point
        MATRIX[KPoints[i].y][KPoints[i].x].cls = to_string(i);

        //Delete selected k point from original points array
        Points.erase(Points.begin() + centrePoint.position);
    }
}

DataPoint MinPoint(DataPoint p1, DataPoint p2) {
    if (p1.x < p2.x || p1.y < p2.y){
        return p1;
    }

    return p2;
}

DataPoint MaxPoint(DataPoint p1, DataPoint p2) {
    if (p1.x > p2.x || p1.y > p2.y){
        return p1;
    }

    return p2;
}

void CalculatePoint(int i) {
    int minDistance = SIZE;
    int minK = 0;
    DataPoint point = Points[i];

    for (int k = 0; k < K; k++) {
        DataPoint kpoint = KPoints[k];
        int distance = PointDistance(point, kpoint);

        //Update min distance and position if a smaller distance is found
        if (distance < minDistance) {
            minDistance = distance;
            minK = k;
        }

    }

    //Update matrix to reflect point with updated cluster group
    MATRIX[point.y][point.x].cls = to_string(minK);

    //Initalise map to keep track of min and max point in k cluster
    if (MinMax.find(minK) == MinMax.end()) {
        MinMax[minK][0] = point;
        MinMax[minK][1] = point;
    }
    //Update map to keep track of min and max point in k cluster
    else  {
        MinMax[minK][0] = MinPoint(MinMax[minK][0], point);
        MinMax[minK][1] = MaxPoint(MinMax[minK][1], point);
    }
}

void* SubRoutine(void *input) {
    //Deattch from main thread
    //pthread_detach(pthread_self());

    //Convert parameter to the correct object
    ThreadArgs* args = (struct ThreadArgs*) input;
    CalculatePoint(args->row);

    //Recuriosn validation to check if any more rows need to be calculated
    if (CURRENT_POINT < Points.size()) {
        //Recursevly call itself with the next row to solve
        int new_row = CURRENT_POINT++;
        args->row = new_row;
        SubRoutine((void *) args);
    }

    CURRENT_POINT = Points.size();

    //Delete the thread from the array and exit thread
    pthread_exit(NULL);
}

void StartRoutine() {
    //Populate thread array
    for (int i = 0; i < MAX_THREADS; i++) {
        pthread_t t;
        threads.push_back(t);
    }

    //Starting threads
    for (int i = 0; i < MAX_THREADS; i++) {
        ThreadArgs *args = (struct ThreadArgs *) malloc(sizeof(struct ThreadArgs));
        args->row = CURRENT_POINT++;
        args->id = i;

        pthread_create(&threads[i], NULL, SubRoutine, (void *) args); 
    }
}

int main() {
    //Populate Array
    PopulateMatrix();

    //Start elapsed time
    struct timespec start, end;
    clock_gettime(CLOCK_MONOTONIC, &start);

    //Block program until all points have been solved by the threads and perform X amount of iterations
    for (int i = 0; i < MAX_ITERATIONS; i++) {
            SelectK();
            StartRoutine();

            //Block code
            while (CURRENT_POINT < Points.size()) {
                continue;
            }

            //Clear threads to avoid memory leaks
            threads.clear();
    }

    clock_gettime(CLOCK_MONOTONIC, &end);

    //Output result matrix
    PrintMatrix();

    //Print elapsed time
    print_timediff("Elapsed Time", start, end);
}