#include "mpi_wrapper.h"
#include <ctime>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <CL/cl.h>
#include <math.h>
#include <string>

const int SIZE = 1000;
#define MAX_NUMBER 100

//NOTE: ARRAY[ROW][COLUMN]
int A[SIZE * SIZE];
int B[SIZE * SIZE];
int ABC[SIZE * SIZE];

int NUM_THREADS = 1;
cl_mem bufA, bufB, bufC;

cl_device_id device_id;
cl_context context;
cl_program program;
cl_kernel kernel; //this is your kernel function
cl_command_queue  queue;
cl_event event = NULL;

int err;

const int max = SIZE;
int maxARows = 0;
const int TS = 2;
//size_t local[2] = { (size_t)TS, (size_t)TS };
size_t local[1] = { (size_t) TS };
size_t global[1]; 

cl_device_id create_device();
void setup_openCL_device_context_queue_kernel(char* filename, char* kernelname);
cl_program build_program(cl_context ctx, cl_device_id dev, const char* filename);
void setup_kernel_memory();
void copy_kernel_args();
void free_memory();

void free_memory() {
   //free the buffers
   clReleaseMemObject(bufA);
   clReleaseMemObject(bufB);
   clReleaseMemObject(bufC);

    //free opencl objects
   clReleaseKernel(kernel);
   clReleaseCommandQueue(queue);
   clReleaseProgram(program);
   clReleaseContext(context);
   
}
void copy_kernel_args() {
    clSetKernelArg(kernel, 0, sizeof(int), (void*)&maxARows);
    clSetKernelArg(kernel, 1, sizeof(int), (void*)&max);
    clSetKernelArg(kernel, 2, sizeof(int), (void*)&max);
    clSetKernelArg(kernel, 3, sizeof(cl_mem), (void*)&bufA);
    clSetKernelArg(kernel, 4, sizeof(cl_mem), (void*)&bufB);
    clSetKernelArg(kernel, 5, sizeof(cl_mem), (void*)&bufC);
    if(err < 0) {
      perror("Couldn't create a kernel argument");
      printf("error = %d", err);
      exit(1);
   }
}
void setup_kernel_memory(int rows) {
     bufA = clCreateBuffer(context, CL_MEM_READ_ONLY,  rows*SIZE*sizeof(int), NULL, NULL);
     bufB = clCreateBuffer(context, CL_MEM_READ_ONLY,  SIZE*SIZE*sizeof(int), NULL, NULL);
     bufC = clCreateBuffer(context, CL_MEM_READ_WRITE, rows*SIZE*sizeof(int), NULL, NULL);

    // Copy matrices to the GPU
    clEnqueueWriteBuffer(queue, bufB, CL_TRUE, 0, SIZE*SIZE*sizeof(int), &B[0], 0, NULL, NULL);
}

void setup_openCL_device_context_queue_kernel(char* filename, char* kernelname) {
    device_id = create_device();
    cl_int err;
    context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &err);
   if(err < 0) {
      perror("Couldn't create a context");
      exit(1);   
    }

    program = build_program(context, device_id, filename );
    queue = clCreateCommandQueueWithProperties(context, device_id, 0, &err);
    if(err < 0) {
      perror("Couldn't create a command queue");
      exit(1);   
    };

    kernel = clCreateKernel(program, kernelname, &err);
    if(err < 0) {
      perror("Couldn't create a kernel");
      printf("error =%d", err);
      exit(1);
    };

}
cl_program build_program(cl_context ctx, cl_device_id dev, const char* filename) {

   cl_program program;
   FILE *program_handle;
   char *program_buffer, *program_log;
   size_t program_size, log_size;
  

   /* Read program file and place content into buffer */
   program_handle = fopen(filename, "r");
   if(program_handle == NULL) {
      perror("Couldn't find the program file");
      exit(1);
   }
   fseek(program_handle, 0, SEEK_END);
   program_size = ftell(program_handle);
   rewind(program_handle);
   program_buffer = (char*)malloc(program_size + 1);
   program_buffer[program_size] = '\0';
   fread(program_buffer, sizeof(char), program_size, program_handle);
   fclose(program_handle);

   /* Create program from file 

   Creates a program from the source code in the add_numbers.cl file. 
   Specifically, the code reads the file's content into a char array 
   called program_buffer, and then calls clCreateProgramWithSource.
   */
   program = clCreateProgramWithSource(ctx, 1, 
      (const char**)&program_buffer, &program_size, &err);
   if(err < 0) {
      perror("Couldn't create the program");
      exit(1);
   }
   free(program_buffer);

   /* Build program 

   The fourth parameter accepts options that configure the compilation. 
   These are similar to the flags used by gcc. For example, you can 
   define a macro with the option -DMACRO=VALUE and turn off optimization 
   with -cl-opt-disable.
   */
   err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
   if(err < 0) {

      /* Find size of log and print to std output */
      clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG, 
            0, NULL, &log_size);
      program_log = (char*) malloc(log_size + 1);
      program_log[log_size] = '\0';
      clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG, 
            log_size + 1, program_log, NULL);
      printf("%s\n", program_log);
      free(program_log);
      exit(1);
   }

   return program;
}

cl_device_id create_device() {

   cl_platform_id platform;
   cl_device_id dev;
   int err;

   /* Identify a platform */
   err = clGetPlatformIDs(1, &platform, NULL);
   if(err < 0) {
      perror("Couldn't identify a platform");
      exit(1);
   } 

   // Access a device
   // GPU
   err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &dev, NULL);
   if(err == CL_DEVICE_NOT_FOUND) {
      // CPU
      err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 1, &dev, NULL);
   }
   if(err < 0) {
      perror("Couldn't access any devices");
      exit(1);   
   }

   return dev;
}

/*
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
struct timespec Start, End;

int GetBlockSize();
int GetBlockStart(int processId);
int GetBlockEnd(int processId);

void PopulateMatrixes() {
    srand(0);

    //Populate array a
    for (int i = 0; i < SIZE; i++) {
        for (int z = 0; z < SIZE; z++) {
            A[i * SIZE + z] = rand() % MAX_NUMBER;
        }
    }

    //Populate array b
    for (int i = 0; i < SIZE; i++) {
        for (int z = 0; z < SIZE; z++) {
            B[i * SIZE + z] = rand() % MAX_NUMBER;
        }
    }
}

void PrintMatrix(std::string title, int matrix[], int m, int n) {
    std::ofstream myfile;
    myfile.open ("./" + title + ".txt");
    //cout << "[Processor " << GetProcessId() << "] " << title << endl;

    for (int i = 0; i < m; i++) {
        for (int z = 0; z < n; z++) {
            myfile << matrix[i * n + z] << ",";
        }

        myfile << std::endl;
    }

    myfile.close();
}

void print_timediff(const char* prefix, const struct timespec& start, const struct timespec& end) {
    double milliseconds = end.tv_nsec >= start.tv_nsec
                        ? (end.tv_nsec - start.tv_nsec) / 1e6 + (end.tv_sec - start.tv_sec) * 1e3
                        : (start.tv_nsec - end.tv_nsec) / 1e6 + (end.tv_sec - start.tv_sec - 1) * 1e3;
    printf("%s: %lf seconds\n", prefix, (milliseconds / 1000));
}

void Calculate(int matrix[], int rows){
    int C[rows * SIZE];
    int processId = GetProcessId();

    //if (processId >= 0) {
        global[0] = (size_t) (rows);
        //global[1] = (size_t) (rows);
        maxARows = rows;

        setup_openCL_device_context_queue_kernel((char*) "./matrix.cl" , (char*) "multiply_matrices");

        setup_kernel_memory(rows);
        clEnqueueWriteBuffer(queue, bufA, CL_TRUE, 0, rows*SIZE*sizeof(int), &matrix[0], 0, NULL, NULL);
        clEnqueueWriteBuffer(queue, bufC, CL_TRUE, 0, rows*SIZE*sizeof(int), &C[0], 0, NULL, NULL);

        copy_kernel_args();

        //submit the kernel for execution 
        clEnqueueNDRangeKernel(queue, kernel, 1, NULL, global, local, 0, NULL, &event);
        clWaitForEvents(1, &event);

        //copying data from the device back to host c matrix
        clEnqueueReadBuffer(queue, bufC, CL_TRUE, 0, rows * SIZE * sizeof(int), &C[0], 0, NULL, NULL);

        //frees memory for device, kernel, queue, etc.
        //you will need to modify this to free your own buffers
        free_memory();
    //}

    if (processId == 0) {
        const int blockStart = GetBlockStart(0);
        int blockEnd = GetBlockEnd(0) + 1 >= SIZE ? GetBlockEnd(0): GetBlockEnd(0) + 1; //Add on to block end to copy last row when performing std::copy(...)
        int blockSize = abs(blockEnd - blockStart);
        std::copy(&C[blockStart * SIZE], &C[0] + blockEnd * SIZE, &ABC[0]); //Copy solved part (matrix C to ABC matrix)
    }

    // if (processId == 0) {
    //     for (int i = 0; i < rows; i++) {
    //         for (int z = 0; z < SIZE; z++) {
    //             //Now calculate 
    //             int res = 0;

    //             for (int iA = 0; iA < SIZE; iA++) {
    //                 res += matrix[i * SIZE + iA] * B[iA * SIZE + z];
    //             }

    //             ABC[i * SIZE + z] = res;
    //             //printf("[%s] C[%d][%d] = %d\n", GetProcessName().c_str(), i, z, res);
    //         }    
    //     }
    // }

    if (processId != 0) {
        MPISend(&C[0], rows * SIZE, MPI_INT, 0, MPI_COMM_WORLD); //Send chunk back to master node
    }

    //PrintMatrix("Sub C Matrix [" + to_string(GetProcessId()) + "]", *C, rows, SIZE); //Output chunk to file (DEBUG PURPOSES)
}

int GetBlockSize(){
    return SIZE / GetProcessesCount(); //Calculate chunk size based on process count
}

int GetBlockStart(int processId){
    const int blockSize = GetBlockSize();
    return processId > 0 ? processId * blockSize + processId : processId * blockSize; //Calculate start of block based on process id
}

int GetBlockEnd(int processId){
    const int blockStart = GetBlockStart(processId);
    const int blockSize = GetBlockSize();
    return blockStart + blockSize > SIZE ? SIZE : blockStart + blockSize; //Calculate end of block based on process id
}

void MasterRoutine() {
    PopulateMatrixes();

    printf("[MASTER] Process Count: %d | Chunk Size: ~%d\n", GetProcessesCount(), GetBlockSize());
    // PrintMatrix("A Matrix", A, SIZE, SIZE);
    // PrintMatrix("B Matrix", B, SIZE, SIZE);
    
    MPIBroadcast(&B, SIZE * SIZE, MPI_INT, 0, MPI_COMM_WORLD); //Broadcast Matrix B to all processes

    //Send chunks to various processes
    for (int i = 0; i < GetProcessesCount(); i++) { 
        if (i == 0) { continue; }
        
        const int blockStart = GetBlockStart(i);
        int blockEnd = GetBlockEnd(i) + 1 >= SIZE ? GetBlockEnd(i): GetBlockEnd(i) + 1; //Add on to block end to copy last row when performing std::copy(...)
        int blockSize = abs(blockEnd - blockStart);

        int SubA[blockSize * SIZE]; 
        std::copy(&A[blockStart * SIZE], &A[0] + blockEnd * SIZE, &SubA[0]); //Copy section from matrix A to sub matrix

        MPISend(&SubA, blockSize * SIZE, MPI_INT, i, MPI_COMM_WORLD); //Send chunk to respective slave process

        if (blockEnd >= SIZE) { break; } //Break loop if you cannot make anymore chunks with avaialble processes
    }

    //Calculate master nodes section
    const int blockStart = GetBlockStart(0);
    int blockEnd = GetBlockEnd(0) + 1 >= SIZE ? GetBlockEnd(0): GetBlockEnd(0) + 1; //Add on to block end to copy last row when performing std::copy(...)
    int blockSize = abs(blockEnd - blockStart);

    clock_gettime(CLOCK_MONOTONIC, &Start);

    int SubA[blockSize * SIZE]; 
    std::copy(&A[blockStart * SIZE], &A[0] + blockEnd * SIZE, &SubA[0]); //Copy section from matrix A to sub matrix
    Calculate(SubA, blockSize);

    //Wait for chunks
    for (int i = 0; i < GetProcessesCount(); i++) {
        if (i == 0) { continue; } //Skip waiting for master results

        const int blockStart = GetBlockStart(i);
        int blockEnd = GetBlockEnd(i) + 1 >= SIZE ? GetBlockEnd(i): GetBlockEnd(i) + 1; //Add on to block end to copy last row when performing std::copy(...)
        int blockSize = abs(blockEnd - blockStart);

        int SubC[blockSize * SIZE]; 
        MPIRecieve(&SubC, blockSize * SIZE, MPI_INT, i, MPI_COMM_WORLD); //Wait for solved chunks from slave nodes

        std::copy(&SubC[0], &SubC[0] + blockSize * SIZE, &ABC[blockStart * SIZE]); //Copy chunk into matrix ABC

        if (blockEnd >= SIZE) { break; } //Break loop if you cannot make anymore chunks with avaialble processes
    }

    clock_gettime(CLOCK_MONOTONIC, &End);
    
    print_timediff("Matrix C:", Start, End);
    PrintMatrix("C Matrix", ABC, SIZE, SIZE); //Output matrix ABC (result) to a file
}

void SlaveRoutine() {
    MPIBroadcast(&B, SIZE * SIZE, MPI_INT, 0, MPI_COMM_WORLD); //Recieve Matrix B from process 0 (Master)
    //printf("[%s- %d] Recieved Matrix B\n", GetProcessName().c_str(), GetProcessId());

    //Calcuate size of incoming array
    int processId = GetProcessId();
    const int blockStart = GetBlockStart(processId);
    int blockEnd = GetBlockEnd(processId) + 1 >= SIZE ? GetBlockEnd(processId): GetBlockEnd(processId) + 1; //Add on to block end to copy last row when performing std::copy(...)
    int blockSize = abs((blockEnd - blockStart));

    int SubA[blockSize * SIZE];

    MPIRecieve(&SubA, blockSize * SIZE, MPI_INT, 0, MPI_COMM_WORLD); //Wait and recieve chunk of matrix a from process 0
    //printf("[%s- %d] Recieved Matrix Sub A\n", GetProcessName().c_str(), GetProcessId());

    Calculate(SubA, blockSize);
    //printf("[%s- %d] Done...\n", GetProcessName().c_str(), GetProcessId());
}

int main() {
    InitializeMPI();

    int processId = GetProcessId();

    //Validate if each processor will do about ~2 rows each
    double averageChunkSize = round((double) SIZE / (double) GetProcessesCount());
    if ((averageChunkSize < 2 || GetProcessesCount() > SIZE) && processId == 0){
        //std::cout << "[SYSTEM] Attempting to use an invalid amount of processors." << endl;
        return 0;
    }    

    if (processId == 0) {
        MasterRoutine();
    }
    else {
        SlaveRoutine();
    }

    FinalizeMPI();

    return 0;
}