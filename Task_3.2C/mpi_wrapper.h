#include <mpi.h>
#include <iostream>
#include <string>

void InitializeMPI() {
    MPI_Init(NULL, NULL); // Initialize the MPI environment
}

int GetProcessesCount() {
    int processCount;
    MPI_Comm_size(MPI_COMM_WORLD, &processCount); // Get the number of processes
    return processCount;
}

int GetProcessId() {    
    int processId;
    MPI_Comm_rank(MPI_COMM_WORLD, &processId); // Get the rank of the process
    return processId;
}

std::string GetProcessName() {
    char processorName[MPI_MAX_PROCESSOR_NAME];
    int nameLen;
    MPI_Get_processor_name(processorName, &nameLen); // Get the name of the processor
    return std::string(processorName);
}

void MPIBroadcast(void *input, int inputSize, MPI_Datatype type, int rootProcessId, MPI_Comm comm){
    MPI_Bcast(input, inputSize, type, rootProcessId, comm); //Broadcast Matrix B
}

void MPISend(void *input, int inputSize, MPI_Datatype type, int destProcessId, MPI_Comm comm){
    MPI_Send(input, inputSize, type, destProcessId, 0, comm); //Send message to particular process
}

void MPIRecieve(void *buffer, int incomingDataCount, MPI_Datatype type, int senderProcessId, MPI_Comm comm){
    MPI_Recv(buffer, incomingDataCount, type, senderProcessId, 0, comm, MPI_STATUS_IGNORE); //Recieve message from particular process
}

void FinalizeMPI(){
    MPI_Finalize(); // Finalize the MPI environment.
}