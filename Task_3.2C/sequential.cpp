#include "mpi_wrapper.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <unordered_map>
#include <ctime> 

using namespace std;

const int K = 30;
const int SIZE = 100;
#define MAX_PERCENT 35
#define MAX_ITERATIONS 500

struct DataPoint {
    int x;
    int y;    
    int cls;
};

struct CentreDataPoint {
    DataPoint point;
    int position;
};

//NOTE: ARRAY[ROW][COLUMN]
DataPoint MATRIX[SIZE][SIZE];
DataPoint SubMatrix[SIZE][SIZE];
vector<DataPoint> Points;
DataPoint KPoints[K];
unordered_map<int, DataPoint[2]> MinMax; 
MPI_Datatype MPI_DATAPOINT;
struct timespec Start, End;

void print_timediff(const char* prefix, const struct timespec& start, const struct timespec& end) {
    double milliseconds = end.tv_nsec >= start.tv_nsec
                        ? (end.tv_nsec - start.tv_nsec) / 1e6 + (end.tv_sec - start.tv_sec) * 1e3
                        : (start.tv_nsec - end.tv_nsec) / 1e6 + (end.tv_sec - start.tv_sec - 1) * 1e3;
    printf("%s: %lf seconds\n", prefix, (milliseconds / 1000));
}

void PopulateMatrix() {
    srand(0);
    //Populate array randomly
    for (long i = 0; i < (SIZE * SIZE) * ((float) MAX_PERCENT) / 100; i++){
        DataPoint dp;
        dp.x = rand() % SIZE;
        dp.y = rand() % SIZE;
        dp.cls = -1;

        //Validation check to not add duplicated points
        if (MATRIX[dp.y][dp.x].cls != -1) {
            MATRIX[dp.y][dp.x] = dp;
            Points.push_back(dp);
        }
        
    }
}

void PrintMatrix(string title) {
    //Open File to dump results
    ofstream myfile;
    myfile.open (title + ".txt");

    for (int i = 0; i < SIZE; i++) {
        for (int z = 0; z < SIZE; z++) {

            //Write current value at MATRIX[i][z] location
            if (MATRIX[i][z].cls == 0) {
                myfile << " , ";
            }
            else {
                myfile << MATRIX[i][z].cls << ", ";
            }
            
        }

        myfile << endl;
    }

    myfile.close();
}

int PointDistance(DataPoint &from, DataPoint &to) {
    int xDifference = to.x - from.x;
    int yDifference = to.y - from.y;
    return sqrt(pow(xDifference, 2) + pow(yDifference, 2));
}

CentreDataPoint SelectCentrePoint(int k, DataPoint &from, DataPoint &to) {
    DataPoint result;
    result.x = (from.x + to.x) / 2;
    result.y = (from.y + to.y) / 2;

    int minDistance = SIZE, minPosition = 0;

    for (int i = 0; i < Points.size(); i++) {
        int distance = PointDistance(result, Points[i]);

        if (distance < minDistance) {
            minDistance = distance;
            minPosition = i;
        }
    }

    CentreDataPoint centreResult;
    centreResult.point = Points[minPosition];
    centreResult.position = minPosition;

    return centreResult;
}

void SelectK() {
    if (MinMax.empty()) {
        //Selecting Inital K Points randomly
        for (int i = 0; i < K; i++) {
            //Set the cluster group in the points array
            int position = rand() % (Points.size() - 1);
            Points[position].cls = i + 1;

            //Update kpoints array with the selected point
            KPoints[i] = Points[position];

            //Update matrix to indicate the selected point as reference point
            MATRIX[KPoints[i].y][KPoints[i].x].cls = i + 1;
            
            //Delete selected k point from original points array
            Points.erase(Points.begin() + position);
        }

        return;
    }

    //Update matrix to indicate the selected k point as reference point
    for (int i = 0; i < K; i++) { 
        MATRIX[KPoints[i].y][KPoints[i].x].cls = i + 1;
        
        //Add original k point back into points array
        Points.push_back(KPoints[i]);

        //Reset current k point with center point of its cluster
        CentreDataPoint centrePoint = SelectCentrePoint(i, MinMax[i][0], MinMax[i][1]);
        KPoints[i] = centrePoint.point;

        //Update matrix to indicate the selected point as reference point
        MATRIX[KPoints[i].y][KPoints[i].x].cls = i + 1;

        //Delete selected k point from original points array
        Points.erase(Points.begin() + centrePoint.position);
    }
}

DataPoint MinPoint(DataPoint p1, DataPoint p2) {
    if (p1.x < p2.x || p1.y < p2.y){
        return p1;
    }

    return p2;
}

DataPoint MaxPoint(DataPoint p1, DataPoint p2) {
    if (p1.x > p2.x || p1.y > p2.y){
        return p1;
    }

    return p2;
}

void Calculate() {
    int minDistance;
    int minK;

    //Inital Clustering
    for (int i = 0; i < Points.size(); i++) {
        //Reset max position and distance
        minK = 0;
        minDistance = SIZE;
        DataPoint point = Points[i];

        for (int k = 0; k < K; k++) {
            DataPoint kpoint = KPoints[k];
            int distance = PointDistance(point, kpoint);

            //Update min distance and position if a smaller distance is found
            if (distance < minDistance) {
                minDistance = distance;
                minK = k;
            }

        }

        //Update matrix to reflect point with updated cluster group
        MATRIX[point.y][point.x].cls = minK + 1;

        //Initalise map to keep track of min and max point in k cluster
        if (MinMax.find(minK) == MinMax.end()) {
            MinMax[minK][0] = point;
            MinMax[minK][1] = point;
        }
        //Update map to keep track of min and max point in k cluster
        else  {
            MinMax[minK][0] = MinPoint(MinMax[minK][0], point);
            MinMax[minK][1] = MaxPoint(MinMax[minK][1], point);
        }
    }
}

void MergeMatrix(DataPoint Sub[SIZE][SIZE]) {
    for (int i = 0; i < SIZE; i++) {
        for (int z = 0; z < SIZE; z++) {
            if (Sub[i][z].cls > 0 && MATRIX[i][z].cls < 1) {
                MATRIX[i][z].cls = Sub[i][z].cls;
            }
        }
    }
}

int GetBlockSize(){
    return (Points.size() / GetProcessesCount()) - 1; //Calculate chunk size based on process count
}

int GetBlockStart(int processId){
    const int blockSize = GetBlockSize();
    return processId > 0 ? processId * blockSize + processId : processId * blockSize; //Calculate start of block based on process id
}

int GetBlockEnd(int processId){
    const int blockStart = GetBlockStart(processId);
    const int blockSize = GetBlockSize();
    return blockStart + blockSize > Points.size() ? Points.size() - 1 : blockStart + blockSize; //Calculate end of block based on process id
}

void HeadNode() {
    //Populate Array
    PopulateMatrix();

    int processId = GetProcessId();
    int processCount = GetProcessesCount();
    int chunkSize = GetBlockSize();

    printf("Process (%d): Process Count=%d | Points Count=%d | Chunk Size=%d | Bytes=%d\n", processId, processCount, (int) Points.size(), chunkSize, (int) sizeof(MATRIX));

    clock_gettime(CLOCK_MONOTONIC, &Start);

    for (int iterations = 0; iterations < MAX_ITERATIONS; iterations++) {
        SelectK();

        MPIBroadcast(&MATRIX, SIZE * SIZE, MPI_DATAPOINT, 0, MPI_COMM_WORLD); //Broadcast matrix
        MPIBroadcast(&KPoints, K, MPI_DATAPOINT, 0, MPI_COMM_WORLD); //Broadcast k values

        //Send chunks from points vector
        for (int i = 0; i < processCount; i++) {
            if (i == 0) continue;

            int chunkStart = GetBlockStart(i);
            int chunkEnd = GetBlockEnd(i);
            int size = abs(chunkStart - chunkEnd) + 1;

            vector<DataPoint> SubPoints(Points.begin() + chunkStart, (Points.begin() + chunkEnd + 1));

            printf("ITER[%d] Process (%d): Start=%d | End=%d | Size=%d\n", iterations, processId, chunkStart, chunkEnd, (int) SubPoints.size());

            MPISend(&size, 1, MPI_INT, i, MPI_COMM_WORLD);
            MPISend(&SubPoints[0], SubPoints.size(), MPI_DATAPOINT, i, MPI_COMM_WORLD);
        }

        int chunkEnd = GetBlockEnd(0);
        vector<DataPoint> tempPoints(Points.begin(), Points.end());
        Points.erase(Points.begin() + chunkEnd + 1, Points.end());
        Calculate();
        Points = tempPoints;

        //Wait for chunks from points vector
        for (int i = 0; i < processCount; i++) {
            if (i == 0) continue;

            MPIRecieve(&SubMatrix[0][0], SIZE * SIZE, MPI_DATAPOINT, i, MPI_COMM_WORLD);
            MergeMatrix(SubMatrix);
            //printf("Process (%d): Recieved Matrix from process %d\n", 0, i);
        }
        
    }

    clock_gettime(CLOCK_MONOTONIC, &End);
    print_timediff("Elapsed Time: ~", Start, End);

    PrintMatrix("Final Result");
}

void SlaveRoutine() {
    int processId = GetProcessId();

    for (int iterations = 0; iterations < MAX_ITERATIONS; iterations++) {
        MPIBroadcast(&MATRIX[0][0], SIZE * SIZE, MPI_DATAPOINT, 0, MPI_COMM_WORLD); //Broadcast matrix
        MPIBroadcast(&KPoints[0], K, MPI_DATAPOINT, 0, MPI_COMM_WORLD); //Broadcast k values
        //printf("Process (%d): Recieved Matrix and inital k values\n", processId);

        //Get size of incoming vector
        int pointsSize;
        MPIRecieve(&pointsSize, 1, MPI_INT, 0, MPI_COMM_WORLD);

        Points.resize(pointsSize);
        MPIRecieve(&Points[0], pointsSize, MPI_DATAPOINT, 0, MPI_COMM_WORLD);

        Calculate();

        MPISend(&MATRIX[0][0], SIZE * SIZE, MPI_DATAPOINT, 0, MPI_COMM_WORLD);
        //printf("Process (%d): Done...\n", processId);
    }
}

int main() {
    InitializeMPI();

    int processId = GetProcessId(); 

    //Create custom mpi data type
    const int nitems = 3;
    int blocklengths[nitems] = {1, 1, 1};
    MPI_Datatype types[nitems] = {MPI_INT, MPI_INT, MPI_INT};
    MPI_Aint offsets[nitems];

    offsets[0] = offsetof(DataPoint, x);
    offsets[1] = offsetof(DataPoint, y);
    offsets[2] = offsetof(DataPoint, cls);

    MPI_Type_create_struct(nitems, blocklengths, offsets, types, &MPI_DATAPOINT);
    MPI_Type_commit(&MPI_DATAPOINT);
    //printf("Process (%d) has comitted DataPoint struct....\n", processId);

    if (processId == 0) {
        HeadNode();
    }
    else {
        SlaveRoutine();
    }

    MPI_Type_free(&MPI_DATAPOINT);
    FinalizeMPI();
}