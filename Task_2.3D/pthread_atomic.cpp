#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <pthread.h> 
#include "print_utili.h"

using namespace std;

int PRODUCER_THREADS;
int CONSUMER_THREADS;
int QUEUE_SIZE;
 
struct TrafficSignal {
    int trafficId;
    int carsPassed;
    long timestamp;
};

struct ThreadArgs {
    int threadId;
    int lineId;
};

//Variables
vector<string> signals;
vector<TrafficSignal> signalQueue;
vector<TrafficSignal> results;
int CURRENT_ROW = 0;

//Thread Variables
vector<pthread_t> producerThreads;
vector<pthread_t> consumerThreads;
pthread_mutex_t accessLock;

vector<string> StringSplit(string phrase, string delimiter){
    vector<string> list;
    string s = string(phrase);
    size_t pos = 0;
    string token;
    while ((pos = s.find(delimiter)) != string::npos) {
        token = s.substr(0, pos);
        list.push_back(token);
        s.erase(0, pos + delimiter.length());
    }
    list.push_back(s);
    return list;
}

void LoadData() {
    ifstream myfile;
    myfile.open("sample_data.txt");

    string line;

    while (getline(myfile, line)) {
        signals.push_back(line);
    }

    //Reverse array 
    reverse(signals.begin(), signals.end());

    CURRENT_ROW = results.size() - 1;

    SmartPrint("[MAIN] " + to_string(signals.size()) + " signals has been loaded.");
}

bool compareSignals(TrafficSignal s1, TrafficSignal s2) { 
    return (s1.carsPassed > s2.carsPassed); 
}

void* ProducerRoutine(void *input) {
    //Cast void pointer to correct data type
    ThreadArgs* args = (struct ThreadArgs*) input;

    while (true) {
        //Exit thread if no more signals to process
        if (signals.size() == 0) {
            pthread_exit(NULL);
        }

        //Lock all producer threads
        pthread_mutex_lock(&accessLock); 

        //Validation for recursive purposes
        if (!signals.empty() && signalQueue.size() < QUEUE_SIZE) {
            string &line = signals[signals.size() - 1];
            signals.pop_back();
            
            //Spilt line from TRAFFIC ID,CARS PASSED,TIMESTAMP
            vector<string> spilts = StringSplit(line, ",");
                
            TrafficSignal sig = TrafficSignal();
            sig.trafficId = atoi(spilts[0].c_str());
            sig.carsPassed = atoi(spilts[1].c_str());
            sig.timestamp = atoi(spilts[2].c_str());

            //Add traffic signal to the queue
            signalQueue.push_back(sig);
            args->lineId = CURRENT_ROW++;
        }

        //Unlock producer threads
        pthread_mutex_unlock(&accessLock);
    }
}

void* ConsumerRoutine(void *input) {
    //Cast void pointer to correct data type
    ThreadArgs* args = (struct ThreadArgs*) input;

    while (true) {
        //Exit thread if zero signals and queue is empty
        if (signals.size() == 0 && signalQueue.size() == 0){
            pthread_exit(NULL);
        }

        //Lock the consumer threads
        pthread_mutex_lock(&accessLock);

        if (signalQueue.size() > 0){
            TrafficSignal sig = signalQueue[0];
            signalQueue.erase(signalQueue.begin());

            bool exists = false;

            //Find the corrosponding traffic id from array and update it
            for (int i = 0; i < results.size(); i++) {
                if (sig.trafficId == results[i].trafficId){
                    results[i].carsPassed += sig.carsPassed;
                    exists = true;
                }
            }
            
            //Add traffic signal to array if it does not exist
            if (exists == false) {
                results.push_back(sig);
            }

            //Sort array
            sort(results.begin(), results.end(), compareSignals);
        }

        //Unlock the consumer threads
        pthread_mutex_unlock(&accessLock);
    }
}

void StartProducers() {
    //Initlaise thread array
    for (int i = 0; i < PRODUCER_THREADS; i++) {
        pthread_t t;
        producerThreads.push_back(t);
    }

    //Starting threads
    for (int i = 0; i < PRODUCER_THREADS; i++) {
        ThreadArgs *args = (struct ThreadArgs *) malloc(sizeof(struct ThreadArgs));
        args->threadId = i;
        args->lineId = CURRENT_ROW++;

        pthread_create(&producerThreads[i], NULL, ProducerRoutine, (void *) args); 
    }
}

void StartConsumers() {
    //Initlaise thread array
    for (int i = 0; i < CONSUMER_THREADS; i++) {
        pthread_t t;
        consumerThreads.push_back(t);
    }

    //Starting threads
    for (int i = 0; i < CONSUMER_THREADS; i++) {
        ThreadArgs *args = (struct ThreadArgs *) malloc(sizeof(struct ThreadArgs));
        args->threadId = i;
        args->lineId = 0;

        pthread_create(&consumerThreads[i], NULL, ConsumerRoutine, (void *) args); 
    }
}

int main() {
    //Initlaise simulation paramaters
    srand(time(NULL));
    PRODUCER_THREADS = (rand() % 8) + 2;
    CONSUMER_THREADS = (rand() % 8) + 2;
    QUEUE_SIZE = (rand() % 900) + 10;

    //Load file into memory
    LoadData();

    //Initilaise mutex
    if (pthread_mutex_init(&accessLock, NULL) != 0) { 
        printf("\n[MAIN]: Access Mutex lock init has failed.\n"); 
        return 1; 
    } 

    //Start producer threads
    StartConsumers();
    StartProducers();
    
    for (int i = 0; i < PRODUCER_THREADS; i++){
        pthread_join(producerThreads[i], NULL);
    }

    for (int i = 0; i < CONSUMER_THREADS; i++){
        pthread_join(consumerThreads[i], NULL);
    }

    //Destory mutex operations
    pthread_mutex_destroy(&accessLock);

    //Print simulation parameters
    cout << endl << "------------ Simulator Parameters (PARRALLEL) ------------" << endl;
    cout << "Producer Thread Count: " << PRODUCER_THREADS << endl;
    cout << "Consumer Thread Count: " << CONSUMER_THREADS << endl;
    cout << "Queue Size Count: " << QUEUE_SIZE << endl;

    //Print popular traffic signal results
    cout << endl << "------------ Popular Traffic Hotspots (PARRALLEL) ------------" << endl;
    for (int i = 0; i < results.size(); i++) {
        cout << "Traffic ID: " + to_string(results[i].trafficId) + " | Number: " + to_string(results[i].carsPassed) + "\n";
    }

    return 1;
}