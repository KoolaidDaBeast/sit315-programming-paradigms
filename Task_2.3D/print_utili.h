#include <iostream>
#include <unordered_map>

using namespace std;

unordered_map<string, int> prints;

void SmartPrint(string str){
    if (prints.find(str) == prints.end()) {
        prints[str] = 0;
        cout << str << endl;
    }
}